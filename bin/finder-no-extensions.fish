#!/opt/homebrew/bin/fish

set showAll (defaults read NSGlobalDomain AppleShowAllExtensions)
if test "$showAll" = "1"
	defaults write NSGlobalDomain AppleShowAllExtensions -bool false && killall -SIGHUP Finder
end
