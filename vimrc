":syntax on
":set expandtab
":set ts=4
":set sw=4
":set softtabstop=4
":set background=light
":set ruler
"au BufRead,BufNewFile *.go set filetype=go
"
"" Enable true colors if available
"set termguicolors
"" colorscheme gruvbox
"" Enable italics, Make sure this is immediately after colorscheme
"" https://stackoverflow.com/questions/3494435/vimrc-make-comments-italic
"highlight Comment cterm=italic gui=italic
