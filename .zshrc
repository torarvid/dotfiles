#zmodload zsh/zprof
# Path to your oh-my-zsh installation.
export ZSH=/Users/tor/.oh-my-zsh

# Set name of the theme to load.
: ${ZSH_THEME:="agnoster-torarvid"}

# Uncomment the following line to use hyphen-insensitive completion. Case
# sensitive completion must be off. _ and - will be interchangeable.
HYPHEN_INSENSITIVE="true"

# Uncomment the following line to display red dots whilst waiting for completion.
COMPLETION_WAITING_DOTS="true"

# Would you like to use another custom folder than $ZSH/custom?
ZSH_CUSTOM=~/dev/dotfiles/ohmyzsh-custom

# export MANPATH="/usr/local/man:$MANPATH"
# Which plugins would you like to load? (plugins can be found in ~/.oh-my-zsh/plugins/*)
# Custom plugins may be added to ~/.oh-my-zsh/custom/plugins/
# Example format: plugins=(rails git textmate ruby lighthouse)
# Add wisely, as too many plugins slow down shell startup.
plugins=(common-aliases git gitfast git-extras gpg-agent)

# User configuration

export DEFAULT_USER="tor"

export PATH=~/bin:~/.local/bin:~/.pyenv/shims:$PATH

export LC_ALL=en_US.UTF-8
export LANG=en_US.UTF-8

source $ZSH/oh-my-zsh.sh
## 
## # Example aliases
## # alias zshconfig="mate ~/.zshrc"
## # alias ohmyzsh="mate ~/.oh-my-zsh"
## export EDITOR=nvim
## export VISUAL=nvim
## 
## function gt() {
##   git tag -a -m "Version v$1" v$1 $2
## }
## 
## function gspri() {
##   git stash
##   grbi $*
##   git stash pop
## }
## 
function gnb() {
  local new_branch=$1
  if [[ "$new_branch" == "" ]]; then
    echo "Must supply a new branch name: gnb <branch_name>"
    return 1
  fi
  git branch $new_branch
  (git diff --quiet)
  local should_stash=$?
  if [[ "$should_stash" != "0" ]]; then
    echo "Stashing uncommitted changes first 🤘🏼..."
    git stash
  fi
  _tracking_branch=$(git rev-parse --abbrev-ref --symbolic-full-name @{u} 2>/dev/null)
  local _track_exit_code=$?
  local tracking_branch=$_tracking_branch
  if [[ "$_track_exit_code" != "0" ]]; then
    echo "No tracking branch. Resetting current branch to 'origin/master'"
    tracking_branch=origin/master
  fi
  git reset --hard $tracking_branch
  git checkout $new_branch
  if [[ "$should_stash" != "0" ]]; then
    echo "Unstashing.."
    git stash pop
  fi
}
## 
## # Check if main exists on origin and use instead of master
## function git_main_origin_branch() {
##   command git rev-parse --git-dir &>/dev/null || return
##   local branch
##   for branch in main trunk; do
##     if command git show-ref -q --verify refs/remotes/origin/$branch; then
##       echo origin/$branch
##       return
##     fi
##   done
##   echo origin/master
## }
## 
## alias gpf="git push --force-with-lease"
## alias gspsp="git stash && gl && git stash pop"
## alias gfrbm='git fetch origin && git rebase $(git_main_origin_branch)'
## alias kubetoken="kubectl -n kube-system get secret \$(kubectl -n kube-system get secret | grep eks-admin | awk '{print \$1}') -o json | jq -r .data.token | decode64"
## alias kc="kubectl"
## alias kp="kubetoken && echo '\nhttp://localhost:8001/api/v1/namespaces/kube-system/services/https:kubernetes-dashboard:/proxy/' && kc proxy"
## alias kcr="kubectl run torarvid-one-off --rm -i --tty --image"
## alias vim=nvim
## 
## function bastion() {
##     case "$1" in
##         blah)
##             # ssh -A -D <port-number> <ssh-host-config>, for example:
##             # ssh -A -D 22220 bst-pro
##             echo 'not configed yet'
##             ;;
##         *)
##             echo "Usage: $0 {not-configured-yet}"
##     esac
## }
## 
## eval $(thefuck --alias)
## 
## # ALT+RIGHTARROW/LEFTARROW to jump words
## # in iTerm2
## bindkey "^[^[[C" forward-word
## bindkey "^[^[[D" backward-word
## # in kitty
## bindkey "\e[1;3D" backward-word
## bindkey "\e[1;3C" forward-word
## 
## unalias rm 2>/dev/null
## unalias fd
## 
## # added by travis gem
## [ -f /Users/tor/.travis/travis.sh ] && source /Users/tor/.travis/travis.sh
## 
## # ~~ Schibsted's One Command Line Tool Setup (v1) ~~
## # Added on 2017-04-27 08:16:56.784815 (UTC)
## # Comment out to disable shell-integration
## [ -e "/Users/tor/.sch/bin/sch-shell-integration-v1.sh" ] \
##   && source "/Users/tor/.sch/bin/sch-shell-integration-v1.sh"
## 
## #THIS MUST BE AT THE END OF THE FILE FOR SDKMAN TO WORK!!!
## export SDKMAN_DIR="/Users/tor/.sdkman"
## [[ -s "/Users/tor/.sdkman/bin/sdkman-init.sh" ]] && source "/Users/tor/.sdkman/bin/sdkman-init.sh"
## 
## export LESS="-F -X $LESS"
## export CLOUDSDK_PYTHON=python3
## # export TERM=xterm-256color
## 
## #zprof
## 
## ulimit -S -n 10000
## 
## # The next line updates PATH for the Google Cloud SDK.
if [ -f '/Users/tor/google-cloud-sdk/path.zsh.inc' ]; then . '/Users/tor/google-cloud-sdk/path.zsh.inc'; fi
## 
## # The next line enables shell command completion for gcloud.
if [ -f '/Users/tor/Downloads/google-cloud-sdk/completion.zsh.inc' ]; then . '/Users/tor/Downloads/google-cloud-sdk/completion.zsh.inc'; fi
## 
## export NVM_DIR="$HOME/.nvm"
## [ -s "/usr/local/opt/nvm/nvm.sh" ] && . "/usr/local/opt/nvm/nvm.sh"  # This loads nvm
## [ -s "/usr/local/opt/nvm/etc/bash_completion.d/nvm" ] && . "/usr/local/opt/nvm/etc/bash_completion.d/nvm"  # This loads nvm bash_completion
## 
## # tabtab source for serverless package
## # uninstall by removing these lines or running `tabtab uninstall serverless`
## [[ -f /Users/tor/dev/dotfiles/config/yarn/global/node_modules/tabtab/.completions/serverless.zsh ]] && . /Users/tor/dev/dotfiles/config/yarn/global/node_modules/tabtab/.completions/serverless.zsh
## # tabtab source for sls package
## # uninstall by removing these lines or running `tabtab uninstall sls`
## [[ -f /Users/tor/dev/dotfiles/config/yarn/global/node_modules/tabtab/.completions/sls.zsh ]] && . /Users/tor/dev/dotfiles/config/yarn/global/node_modules/tabtab/.completions/sls.zsh
## # tabtab source for slss package
## # uninstall by removing these lines or running `tabtab uninstall slss`
## [[ -f /Users/tor/dev/dotfiles/config/yarn/global/node_modules/tabtab/.completions/slss.zsh ]] && . /Users/tor/dev/dotfiles/config/yarn/global/node_modules/tabtab/.completions/slss.zsh
## 
## eval "$(pyenv init -)"
## eval "$(direnv hook zsh)"
## 
## test -e "${HOME}/.iterm2_shell_integration.zsh" && source "${HOME}/.iterm2_shell_integration.zsh"
## 
## # autocomplete for kc
## source <(kubectl completion zsh)
## complete -F __start_kubectl kc
## alias klipy="~/oda/klipy/.venv/bin/python -m klipy"
## 
## [ -f ~/.fzf.zsh ] && source ~/.fzf.zsh
## 
## eval "$(starship init zsh)"
