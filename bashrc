# path
export PATH=$HOME/bin:/usr/local/bin:/usr/local/sbin:$PATH

# git completion
. /usr/local/etc/bash_completion.d/git-completion.bash

green=$(tput setaf 2)
red=$(tput setaf 1)
blue=$(tput setaf 4)
bold=$(tput bold)
reset=$(tput sgr0)

export PS1='\[$blue\]\W\[$red\]\$\[$reset\] '

export LC_ALL=nb_NO.UTF-8
export LANG=nb_NO.UTF-8
export EDITOR=vim
export VISUAL=vim
