if status is-interactive
    zoxide init fish | source
    set script_dir (status dirname)
    source $script_dir/functions/thefuck.fish
end

fish_vi_key_bindings
set fish_cursor_default block
set fish_cursor_insert line blink
set fish_cursor_replace_one underscore blink
set fish_cursor_visual block

set -U fish_greeting "🐟"
