function fish_right_prompt

    if not set -q __fish_color_green
        set -g __fish_color_green (set_color green)
    end

    set -g __fish_git_prompt_showdirtystate 1
    set -g __fish_git_prompt_showstashstate 1
    set -g __fish_git_prompt_showuntrackedfiles 1
    set -g __fish_git_prompt_showcolorhints 1
    set -g __fish_git_prompt_showupstream informative
    #set -g __fish_git_prompt_show_informative_status 1 #slow

    echo -n (__fish_git_prompt)
    echo -n "$__fish_color_red" "["(date '+%m/%d/%y %H:%M:%S')"]"
    echo -n "$__fish_prompt_normal"
end
