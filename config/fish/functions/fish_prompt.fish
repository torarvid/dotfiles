function fish_prompt --description 'Write out the prompt'

    set -g last_status $status

    # Just calculate these once, to save a few cycles when displaying the prompt
    if not set -q __fish_prompt_hostname
        set -g __fish_prompt_hostname (hostname|cut -d . -f 1)
    end

    if not set -q __fish_prompt_normal
        set -g __fish_prompt_normal (set_color normal)
    end

    if not set -q __fish_color_yellow
        set -g __fish_color_yellow (set_color FF0)
    end

    if not set -q __fish_color_purple
        set -g __fish_color_purple (set_color purple)
    end

    if not set -q __fish_color_red
        set -g __fish_color_red (set_color red)
    end

    #Set the color for the status depending on the value
    if not test $last_status -eq 0
        set __fish_color_status (set_color red)
    else
        set __fish_color_status (set_color green)
    end

    switch $USER

    case root
        if not set -q __fish_prompt_cwd
            if set -q fish_color_cwd_root
                set -g __fish_prompt_cwd (set_color $fish_color_cwd_root)
            else
                set -g __fish_prompt_cwd (set_color $fish_color_cwd)
            end
        end

        printf '%s@%s %s%s%s# ' $USER $__fish_prompt_hostname "$__fish_prompt_cwd" (prompt_pwd) "$__fish_prompt_normal"

    case '*'
        if not set -q __fish_prompt_cwd
            set -g __fish_prompt_cwd (set_color $fish_color_cwd)
        end

        printf '\n%s%s %s%s%s %s$status=(%s)%s\f\r> ' "$__fish_color_red" $USER "$__fish_prompt_cwd" "$__fish_color_yellow" (pwd) "$__fish_color_status" "$last_status" "$__fish_prompt_normal"

    end
end
