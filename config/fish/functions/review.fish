function review
    if not count $argv > /dev/null
        echo "Usage: review <repo>"
        return 1
    end

    set repo "kolonialno/$argv[1]"
    set prs (gh pr list -R $repo -S "user-review-requested:@me sort:updated-asc is:open" --json url,title,author,reviews,createdAt,updatedAt)
    set pr (echo $prs | jq '. | sort_by(.author.is_bot) | first')
    if test "$pr" = "null"
        echo "No PRs to review"
        return 0
    end

    set pr_id (echo $pr | jq -r '.url | split("/") | [.[-3, -1]] | join("#")')
    set author (echo $pr | jq -r '.author.name')
    set date (echo $pr | jq -r '.updatedAt | fromdate | strftime("%A, %b %d at %H:%M")')
    set title (echo $pr | jq '.title')
    set url (echo $pr | jq -r '.url')

    echo "Opening $pr_id $title by $author from $date..."

    if command -v open >/dev/null
        open "$url"
    else
        xdg-open "$url"
    end
end
