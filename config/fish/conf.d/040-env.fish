set --export EDITOR nvim
set --export VISUAL nvim
set --export SHELL (which fish)
set --export CLOUDSDK_PYTHON_SITEPACKAGES 1
set --export PISTOL_CHROMA_STYLE native
set --export HOMEBREW_PREFIX /opt/homebrew

set --export XDG_CONFIG_HOME ~/.config
set --export XDG_CACHE_HOME ~/.cache
set --export XDG_DATA_HOME ~/.local/share
set --export XDG_BIN_HOME ~/.local/bin
set --export XDG_STATE_HOME ~/.local/state
set --export USE_GKE_GCLOUD_AUTH_PLUGIN True

set --export GOBIN ~/.local/bin
set --export CARGO_INSTALL_ROOT ~/.local

set --export UV_KEYRING_PROVIDER subprocess
set --export UV_INDEX_NUBE_PYTHON_USERNAME oauth2accesstoken
set --export PYTHON_KEYRING_BACKEND keyring_gcloud.GoogleCloudKeyring

set --export JJ_CONFIG ~/.config/jj

set --export ANDROID_HOME ~/Library/Android/sdk
fish_add_path $ANDROID_HOME/emulator
fish_add_path $ANDROID_HOME/platform-tools

set --export DIRENV_LOG_FORMAT ""

if type -q ov
    set --export PSQL_PAGER 'ov -F -C -d "|" -H1 --column-rainbow'
    set --export MANPAGER ov
end

function kebab-to-env --description 'Convert a lowercase kebab-string to an uppercase env-string'
    echo $argv | tr '-' '_' | tr '[:lower:]' '[:upper:]'
end

for file in ~/.scorerc/*
    if test -f $file
        set key (echo (kebab-to-env (basename $file)))
        set --export $key (cat $file)
    end
end

fish_add_path $HOMEBREW_PREFIX/bin
fish_add_path ~/bin
fish_add_path ~/.local/bin
