function git_main_branch --description 'Find the main branch in a git repo'
	git rev-parse --git-dir &>/dev/null || return
	for ref in refs/{heads,remotes/{origin,upstream}}/{main,trunk}
		if git show-ref -q --verify $ref
			string split --right --field 2 --max 1 / {$ref}
			return
		end
	end
	echo master
end

function git_main_origin_branch --description 'Find main branch at the origin'
	git rev-parse --git-dir &>/dev/null || return
	for branch in main trunk
		if command git show-ref -q --verify refs/remotes/origin/$branch
			echo origin/$branch
			return
		end
	end
	echo origin/master
end

function gnb --description 'Give your git work a new branch name (git-new-branch)'
	set new_branch $argv[1]
	if test "$new_branch" = ""
		echo "Must supply a new branch name: gnb <branch_name>"
		return 1
	end
	git branch $new_branch
	git diff --quiet
	if test "$status" != "0"
		echo "Stashing uncommitted changes first 🤘🏼..."
		git stash
		set should_pop_stash "yes"
	end
	set main_branch (git_main_origin_branch)
	set tracking_branch (git rev-parse --abbrev-ref --symbolic-full-name @{u} 2>/dev/null)
	if test "$status" != "0"
		echo "No tracking branch. Resetting current branch to '$main_branch'"
		set tracking_branch $main_branch
	end
	git reset --hard $tracking_branch
	git checkout $new_branch
	if test "$should_pop_stash" = "yes"
		echo "Unstashing.."
		git stash pop
	end
end

function lw --description 'Show the location of a command'
    ls -la (which $argv)
end

function smartcd --description 'cd into something using fzf'
    set starting_path $argv[1]
    set max_depth $argv[2]
    set query $argv[3..-1]

    if test "$query" != ""
        set query "-q $query"
    end

    set new_path (fd . $starting_path --maxdepth $max_depth --hidden --follow --type directory | fzf $query)
    if test "$new_path" != ""
        cd $new_path
    end
end
