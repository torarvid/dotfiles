alias cdd='smartcd ~/dev 3'
alias cdh='smartcd ~/ 5'
alias cdo='smartcd ~/oda 3'
alias gfdba="gfrbm && gcm && git rebase (git_main_origin_branch) && gbda"
alias icat="kitty +kitten icat"
alias ls="gls --color"
alias nvimc="pushd ~/.config/nvim && nvim && popd"
alias trex-rd1-prod="LOG_MESSAGES=True poetry run python -m trex main  --fulfillment-center-model=rd1 --rabbitmq-url=rabbitmq://127.0.0.1:5672 --wms-use-dummy --generate-sample-totes 200 2>&1 | tee -a main-(date '+%Y%m%d-%H%M%S').log"
alias vimf="pushd ~/.config/fish && vim && popd"
