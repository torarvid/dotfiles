if not status is-interactive
    return
end
if type -q gcloud
    set GCLOUD_PREFIX (realpath (which gcloud)/../..)
    if test -f $GCLOUD_PREFIX/path.fish.inc
        source $GCLOUD_PREFIX/path.fish.inc
    end
end
