vim.lsp.set_log_level("info")

local vmap = function(keys, func, desc)
    if desc then
        desc = "RustLsp: " .. desc
    end

    vim.keymap.set("v", keys, func, { buffer = bufnr, desc = desc })
end

local nmap = function(keys, func, desc)
    if desc then
        desc = "RustLsp: " .. desc
    end

    vim.keymap.set("n", keys, func, { buffer = bufnr, desc = desc })
end

local telescope = require("telescope.builtin")

nmap("<leader>cr", vim.lsp.buf.rename, "[C]ode [r]ename")
nmap("<leader>ca", ":RustLsp codeAction<CR>", "[C]ode [A]ction")
vmap("<C-A>", ":RustLsp codeAction<CR>", "[C]ode [A]ction")

nmap("gd", telescope.lsp_definitions, "[G]oto [D]efinition")
nmap("gr", telescope.lsp_references, "[G]oto [R]eferences")
nmap("gI", telescope.lsp_implementations, "[G]oto [I]mplementation")
nmap("<leader>ft", telescope.lsp_type_definitions, "Type [D]efinition")
nmap("<leader>fS", telescope.lsp_document_symbols, "[D]ocument [S]ymbols")
nmap("<leader>fs", telescope.lsp_dynamic_workspace_symbols, "[W]orkspace [S]ymbols")
nmap("<leader>fc", telescope.lsp_incoming_calls, "[I]ncoming [C]alls")
nmap("<leader>fC", telescope.lsp_outgoing_calls, "[O]utgoing [C]alls")

-- See `:help K` for why this keymap
nmap("K", vim.lsp.buf.hover, "Hover Documentation")
nmap("<C-k>", vim.lsp.buf.signature_help, "Signature Documentation")

-- Lesser used LSP functionality
nmap("gD", vim.lsp.buf.declaration, "[G]oto [D]eclaration")
nmap("<leader>wa", vim.lsp.buf.add_workspace_folder, "[W]orkspace [A]dd Folder")
nmap("<leader>wr", vim.lsp.buf.remove_workspace_folder, "[W]orkspace [R]emove Folder")
nmap("<leader>wl", function()
    P(vim.lsp.buf.list_workspace_folders())
end, "[W]orkspace [L]ist Folders")

-- Create a command `:Format` local to the LSP buffer
-- get bufnr for current buffer
local bufnr = vim.api.nvim_get_current_buf()
vim.api.nvim_buf_create_user_command(bufnr, "Format", function(_)
    vim.lsp.buf.format()
end, { desc = "Format current buffer with LSP" })
nmap("<leader>cf", "<cmd>Format<cr>", "[C]ode [f]ormat")
