local function augroup(name)
  return vim.api.nvim_create_augroup("torarvid_" .. name, { clear = true })
end

-- Override ftplugin default and make comment leader not be auto-inserted on
-- <Enter> and o/O
vim.api.nvim_create_autocmd("FileType", {
  group = augroup("no_auto_comment_leader"),
  pattern = { "*" },
  callback = function()
    vim.opt_local.formatoptions:remove({ "r", "o" })
  end,
})

-- Set text width to 88 by default for all Python files
-- vim.api.nvim_create_autocmd("FileType", {
--   group = augroup("python_text_width"),
--   pattern = { "python" },
--   callback = function()
--     vim.cmd("setlocal textwidth=88")
--   end,
-- })

vim.api.nvim_create_autocmd('TextYankPost', {
  callback = function()
    vim.highlight.on_yank()
  end,
  group = augroup("YankHighlight"),
  pattern = '*',
})

