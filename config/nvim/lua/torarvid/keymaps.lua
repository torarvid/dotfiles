local kh = require("torarvid.keys")
local nmap = kh.nmap
local imap = kh.imap
local vmap = kh.vmap
local taf = require("torarvid.functions")

-- Have jk or kj function as Esc in insert mode
imap({ "jk", "<Esc>" })
imap({ "kj", "<Esc>" })

-- Tab in normal mode swaps buffers
nmap({ "<Leader><Tab>", ":bnext<CR>" })
nmap({ "<Leader><S-Tab>", ":bprevious<CR>" })

-- Easy luafile source
nmap({ "<Leader>ll", ":w<CR>:luafile %<CR>" })

-- Better indent/outdent
vmap({ "<Tab>", ">gv" })
vmap({ "<S-Tab>", "<gv" })

-- Toggle relative/abs line numbers
nmap({ "<Leader>tl", taf.toggle_numbers })

-- Markdown paste
nmap({ "<Leader>mdp", taf.markdown_paste })

-- Copy current file's relative path
nmap({ "<Leader>cp", taf.copy_current_path })

-- Quickfix list nav
nmap({ "<Leader>qn", "<cmd>cnext<CR>" })
nmap({ "<D-S-j>", "<cmd>cnext<CR>" })
nmap({ "<Leader>qp", "<cmd>cprevious<CR>" })
nmap({ "<D-S-k>", "<cmd>cprevious<CR>" })
nmap({ "<Leader>qc", "<cmd>cclose<CR>" })

-- LazyGit
nmap({ "<Leader>gg", "<cmd>lua Snacks.lazygit()<CR>", { desc = "Start lazygit" } })
nmap({
    "<Leader>gG",
    ":LazyGitCurrentFile<CR>",
    { desc = "Start lazygit (current file)" },
})

-- Set current working directory to that of the current file
nmap({ ",cd", ":cd %:p:h<CR>", { desc = "Set cwd to current file" } })
