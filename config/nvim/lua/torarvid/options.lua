vim.opt.tabstop = 4
vim.opt.shiftwidth = 4

vim.opt.formatoptions:remove({ "r", "o" }) -- don't insert comment leader when hitting <Enter> or o/O
vim.opt.formatoptions:remove({ "t", "c" }) -- don't wrap text/comments on textwidth

-- vim.opt.listchars = "tab:» ,trail:·,extends:→,precedes:↑,nbsp:☠,conceal:┊"
vim.opt.listchars = {
    tab = "» ",
    trail = "·",
    extends = "→",
    precedes = "↑",
    nbsp = "☠",
    conceal = "┊",
}
vim.opt.list = true -- show whitespace

vim.opt.conceallevel = 1 -- concealed text has each char replaced

vim.opt.cursorline = true -- highlight current line
vim.g.mapleader = " "

-- Set highlight on search
vim.o.hlsearch = false

-- Make line numbers default
vim.wo.number = true

-- And use relative line numbers in the gutter
vim.wo.relativenumber = true

-- Enable mouse mode
vim.o.mouse = "a"

-- If 0, hide the vim command line when not in use
vim.o.cmdheight = 1

-- Sync clipboard between OS and Neovim.
vim.o.clipboard = "unnamedplus"

-- Enable break indent
vim.o.breakindent = false

-- Save undo history
vim.o.undofile = true

-- Case-insensitive searching UNLESS \C or capital in search
vim.o.ignorecase = true
vim.o.smartcase = true

-- Keep signcolumn on by default
vim.wo.signcolumn = "auto"

-- Decrease update time
vim.o.updatetime = 250
vim.o.timeoutlen = 300

-- Set completeopt to have a better completion experience
vim.o.completeopt = "menuone,noselect"

-- NOTE: You should make sure your terminal supports this
vim.o.termguicolors = true

-- [[ Basic Keymaps ]]

-- Keymaps for better default experience
-- See `:help vim.keymap.set()`
vim.keymap.set({ "n", "v" }, "<Space>", "<Nop>", { silent = true })

-- Remap for dealing with word wrap
vim.keymap.set("n", "k", "v:count == 0 ? 'gk' : 'k'", { expr = true, silent = true })
vim.keymap.set("n", "j", "v:count == 0 ? 'gj' : 'j'", { expr = true, silent = true })

-- Diagnostic keymaps
vim.keymap.set("n", "[d", vim.diagnostic.goto_prev, { desc = "Go to previous diagnostic message" })
vim.keymap.set("n", "]d", vim.diagnostic.goto_next, { desc = "Go to next diagnostic message" })
vim.keymap.set("n", "<leader>d", vim.diagnostic.open_float, { desc = "Open floating diagnostic message" })
vim.keymap.set("n", "<leader>q", vim.diagnostic.setloclist, { desc = "Open diagnostics list" })
