local M = {}

M.toggle_numbers = function()
  vim.opt.relativenumber = not vim.opt.relativenumber._value
end

M.markdown_paste = function(link)
  local curl = require("plenary.curl")

  link = link or vim.fn.getreg("+")

  if not vim.startswith(link, "https://") then
    return
  end

  local request = curl.get(link)
  if not request then
    print("request failed")
    return
  end

  if request.status ~= 200 then
    print("Failed to get link")
    return
  end

  local html_parser = vim.treesitter.get_string_parser(request.body, "html")
  if not html_parser then
    print("Must have html parser installed")
    return
  end

  local tree = (html_parser:parse() or {})[1]
  if not tree then
    print("Failed to parse tree")
    return
  end

  local query = vim.treesitter.query.parse(
    "html",
    [[
      (
       (element
        (start_tag
         (tag_name) @tag)
        (text) @text
       )
       (#eq? @tag "title")
      )
    ]]
  )

  for id, node in query:iter_captures(tree:root(), request.body, 0, -1) do
    local name = query.captures[id]
    if name == "text" then
      local title = vim.treesitter.get_node_text(node, request.body)
      vim.api.nvim_input(string.format("a[%s](%s)", title, link))
      return
    end
  end
end

M.copy_current_path = function()
    local relative_path = vim.fn.fnamemodify(vim.fn.expand("%"), ":.")
    vim.fn.setreg('+', relative_path)
    print("Copied to clipboard: " .. relative_path)
end

-- vim.api.nvim_set_keymap('n', '<leader>yr', ':lua copy_relative_path()<CR>', {noremap = true, silent = true})

return M
