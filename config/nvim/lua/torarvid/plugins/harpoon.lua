local nmap = require("torarvid.keys").nmap

nmap({
  "<leader>ha",
  '<cmd>lua require("harpoon.mark").add_file()<CR>',
  { desc = "Bookmark file" },
})
nmap({ "<leader>hm", '<cmd>lua require("harpoon.ui").toggle_quick_menu()<CR>', { desc = "Harpoon menu" } })
nmap({ "<leader>1", '<cmd>lua require("harpoon.ui").nav_file(1)<CR>', { desc = "Goto file 1" } })
nmap({ "<leader>2", '<cmd>lua require("harpoon.ui").nav_file(2)<CR>', { desc = "Goto file 2" } })
nmap({ "<leader>3", '<cmd>lua require("harpoon.ui").nav_file(3)<CR>', { desc = "Goto file 3" } })
nmap({ "<leader>4", '<cmd>lua require("harpoon.ui").nav_file(4)<CR>', { desc = "Goto file 4" } })
nmap({ "<leader>5", '<cmd>lua require("harpoon.ui").nav_file(5)<CR>', { desc = "Goto file 5" } })
nmap({ "<leader>6", '<cmd>lua require("harpoon.ui").nav_file(6)<CR>', { desc = "Goto file 6" } })
nmap({ "<leader>7", '<cmd>lua require("harpoon.ui").nav_file(7)<CR>', { desc = "Goto file 7" } })
nmap({ "<leader>8", '<cmd>lua require("harpoon.ui").nav_file(8)<CR>', { desc = "Goto file 8" } })
nmap({ "<leader>9", '<cmd>lua require("harpoon.ui").nav_file(9)<CR>', { desc = "Goto file 9" } })
nmap({ "<leader>j", '<cmd>lua require("harpoon.ui").nav_next()<CR>', { desc = "Goto next file" } })
nmap({ "<leader>k", '<cmd>lua require("harpoon.ui").nav_prev()<CR>', { desc = "Goto prev file" } })

return { { "ThePrimeagen/harpoon" } }
