return {
    {
        "nvim-neotest/neotest",
        dependencies = {
            "nvim-lua/plenary.nvim",
            "antoinemadec/FixCursorHold.nvim",
            "nvim-treesitter/nvim-treesitter",
            "nvim-neotest/neotest-python",
        },
        config = function(opts)
            opts.adapters = {
                require("neotest-python")({
                    dap = { justMyCode = false }
                }),
                require('rustaceanvim.neotest')
            }
            require("neotest").setup(opts)
        end,
        keys = {
            { "<Leader>tn", "<Cmd>lua require('neotest').run.run()<CR>", { desc = "Neotest: Run nearest test" } },
            { "<Leader>tf", "<Cmd>lua require('neotest').run.run(vim.fn.expand('%'))<CR>", { desc = "Neotest: Run file" } },
            { "<Leader>td", "<Cmd>lua require('neotest').run.run({ strategy = 'dap' })<CR>", { desc = "Neotest: Debug nearest test" } },
            { "<Leader>ts", "<Cmd>lua require('neotest').run.stop()<CR>", { desc = "Neotest: Stop" } },
        },
    }
}
