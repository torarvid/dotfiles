return {
    {
        "torarvid/nvim-pytrize.lua",
        ft = "python",
        config = true,
        keys = {
            { "<leader>gx", "<cmd>PytrizeJumpFixture<CR>", "[G]oto fi[x]ture" },
        }
    },
}
