return {
    {
        "nvim-lualine/lualine.nvim",
        opts = {
            options = {
                icons_enabled = true,
                component_separators = "|",
                section_separators = "",
            },
        },
    },
    {
        "nvim-neo-tree/neo-tree.nvim",
        config = true,
        dependencies = {
            "nvim-lua/plenary.nvim",
            "nvim-tree/nvim-web-devicons", -- not strictly required, but recommended
            "MunifTanjim/nui.nvim",
            "3rd/image.nvim", -- Optional image support in preview window: See `# Preview Mode` for more information
        },
        keys = {
            -- Open Neotree with directory of current file selected
            { "<Leader>e", "<cmd>Neotree toggle reveal<CR>", desc = "Neotree" },
            { "<Leader>fx", "<cmd>Neotree dir=%:p:h<CR>", desc = "Neotree current file" },
        },
    },
}
