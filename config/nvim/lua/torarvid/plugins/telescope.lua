return {
    {
        "nvim-telescope/telescope.nvim",
        lazy = true,
        cmd = "Telescope",
        opts = {
            defaults = { layout_config = { width = 0.95, height = 0.95 } },
            pickers = {
                find_files = {
                    find_command = {
                        "fd",
                        "--hidden",
                        "--type",
                        "file",
                    },
                },
                live_grep = {
                    vimgrep_arguments = {
                        "rg",
                        "--hidden",
                        "--no-heading",
                        "--with-filename",
                        "--color",
                        "never",
                        "--line-number",
                        "--column",
                        "--sort",
                        "path",
                    },
                },
            },
        },
        dependencies = {
            {"nvim-lua/plenary.nvim"},
            {
                "nvim-telescope/telescope-fzf-native.nvim",
                build = "make",
                config = function()
                    require("telescope").load_extension("fzf")
                end,
            },
        },
        keys = {
            {"<leader><space>", "<cmd>Telescope find_files<cr>", desc = "Find files"},
            {"<leader>ff", "<cmd>Telescope git_files<cr>", desc = "Git files"},
            {"<leader>f/", "<cmd>Telescope live_grep<cr>", desc = "Live grep"},
            {"<leader>fr", "<cmd>Telescope resume<cr>", desc = "Resume search"},
            {"<leader>fk", "<cmd>Telescope keymaps<cr>", desc = "Keymaps"},
        },
    },
}


