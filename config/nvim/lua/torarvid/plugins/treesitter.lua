return {
    {
        "vrischmann/tree-sitter-templ",
        event = "BufRead",
        dependencies = { "nvim-treesitter/nvim-treesitter" },
    },
    {
        -- treesitter-context enables sticky scroll
        "nvim-treesitter/nvim-treesitter-context",
        event = "BufRead",
        dependencies = { "nvim-treesitter/nvim-treesitter" },
    },
    {
        "nvim-treesitter/nvim-treesitter-textobjects",
        event = "BufRead",
        dependencies = { "nvim-treesitter/nvim-treesitter" },
    },
    {
        "nvim-treesitter/nvim-treesitter",
        build = ":TSUpdate",
        event = "VeryLazy",
        config = function()
            local configs = require("nvim-treesitter.configs")

            configs.setup({
                modules = {},
                ignore_install = {},
                auto_install = false,
                ensure_installed = {
                    "c",
                    "d",
                    "dot",
                    "elixir",
                    "fish",
                    "gitcommit",
                    "gitignore",
                    "go",
                    "gomod",
                    "gosum",
                    "gowork",
                    "graphql",
                    "html",
                    "javascript",
                    "julia",
                    "kotlin",
                    "latex",
                    "lua",
                    "make",
                    "nix",
                    "python",
                    "ruby",
                    "rust",
                    "svelte",
                    "swift",
                    "templ",
                    "toml",
                    "vim",
                    "vimdoc",
                    "vue",
                    "zig",
                    "sql",
                },
                sync_install = false,
                highlight = { enable = true },
                indent = { enable = true },
                textobjects = {
                    select = {
                        enable = true,
                        lookahead = true, -- Automatically jump forward to textobj, similar to targets.vim
                        keymaps = {
                            -- You can use the capture groups defined in textobjects.scm
                            ["aa"] = "@parameter.outer",
                            ["ia"] = "@parameter.inner",
                            ["af"] = "@function.outer",
                            ["if"] = "@function.inner",
                            ["ac"] = "@class.outer",
                            ["ic"] = "@class.inner",
                        },
                    },
                    move = {
                        enable = true,
                        set_jumps = true, -- whether to set jumps in the jumplist
                        goto_next_start = {
                            ["]m"] = "@function.outer",
                            ["]]"] = "@class.outer",
                        },
                        goto_next_end = {
                            ["]M"] = "@function.outer",
                            ["]["] = "@class.outer",
                        },
                        goto_previous_start = {
                            ["[m"] = "@function.outer",
                            ["[["] = "@class.outer",
                        },
                        goto_previous_end = {
                            ["[M"] = "@function.outer",
                            ["[]"] = "@class.outer",
                        },
                    },
                    swap = {
                        enable = true,
                        swap_next = {
                            ["<leader>a"] = "@parameter.inner",
                        },
                        swap_previous = {
                            ["<leader>A"] = "@parameter.inner",
                        },
                    },
                },
            })
        end,
    },
}
