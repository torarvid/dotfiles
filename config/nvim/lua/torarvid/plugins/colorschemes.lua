return {
    { "tiagovla/tokyodark.nvim", lazy = true },
    { "scottmckendry/cyberdream.nvim", lazy = true },
    {
        "rebelot/kanagawa.nvim",
        lazy = false,
        priority = 100,
        config = function()
            local overrides = function(colors)
                return { Comment = { fg = colors.palette.springBlue } }
            end
            require("kanagawa").setup({ overrides = overrides })
        end,
    },
    {
        "eldritch-theme/eldritch.nvim",
        lazy = false,
        priority = 10,
    },
    {
        "trusktr/seti.vim",
        lazy = false,
        priority = 10,
        config = function()
            -- For Telescope match pane
            vim.api.nvim_set_hl(0, "TelescopeSelection", { bg = "#8e7451" })

            -- For Telescope preview pane
            vim.api.nvim_set_hl(0, "Visual", { bg = "#8e7451" })

            -- For built-in selection lists
            vim.api.nvim_set_hl(0, "PmenuSel", { bg = "#8e7451" })

            -- For the current buffer line
            vim.api.nvim_set_hl(0, "CursorLine", { bg = "#444444" })
        end,
    },
    {
        "ribru17/bamboo.nvim",
        lazy = false,
        priority = 1000,
        config = function()
            require("bamboo").setup({
                -- optional configuration here
            })
            require("bamboo").load()
            vim.cmd("colorscheme bamboo")

            -- For Telescope match pane
            vim.api.nvim_set_hl(0, "TelescopeSelection", { bg = "#8e7451" })

            -- For Telescope preview pane
            vim.api.nvim_set_hl(0, "Visual", { bg = "#8e7451" })

            -- For built-in selection lists
            vim.api.nvim_set_hl(0, "PmenuSel", { bg = "#8e7451" })

            -- For the current buffer line
            vim.api.nvim_set_hl(0, "CursorLine", { bg = "#444444" })
        end,
    },
}
