-- Uncomment if needed
-- vim.lsp.set_log_level("info")

local nmap = function(keys, func, desc)
    if desc then
        desc = "LSP: " .. desc
    end

    vim.keymap.set("n", keys, func, { buffer = bufnr, desc = desc })
end

local vmap = function(keys, func, desc)
    if desc then
        desc = "LSP: " .. desc
    end

    vim.keymap.set("v", keys, func, { buffer = bufnr, desc = desc })
end

local on_attach_overrides = {}

local on_attach = function(opts, bufnr)
    local telescope = require("telescope.builtin")
    nmap("<leader>cr", vim.lsp.buf.rename, "[C]ode [r]ename")
    nmap("<leader>ca", vim.lsp.buf.code_action, "[C]ode [A]ction")
    vmap("<C-A>", vim.lsp.buf.code_action, "[C]ode [A]ction")

    nmap("gd", telescope.lsp_definitions, "[G]oto [D]efinition")
    nmap("gr", telescope.lsp_references, "[G]oto [R]eferences")
    nmap("gI", telescope.lsp_implementations, "[G]oto [I]mplementation")
    nmap("<leader>ft", telescope.lsp_type_definitions, "Type [D]efinition")
    nmap("<leader>fS", telescope.lsp_document_symbols, "[D]ocument [S]ymbols")
    nmap("<leader>fs", telescope.lsp_dynamic_workspace_symbols, "[W]orkspace [S]ymbols")
    nmap("<leader>fc", telescope.lsp_incoming_calls, "[I]ncoming [C]alls")
    nmap("<leader>fC", telescope.lsp_outgoing_calls, "[O]utgoing [C]alls")

    -- See `:help K` for why this keymap
    nmap("K", vim.lsp.buf.hover, "Hover Documentation")
    nmap("<C-k>", vim.lsp.buf.signature_help, "Signature Documentation")

    -- Lesser used LSP functionality
    nmap("gD", vim.lsp.buf.declaration, "[G]oto [D]eclaration")
    nmap("<leader>wa", vim.lsp.buf.add_workspace_folder, "[W]orkspace [A]dd Folder")
    nmap("<leader>wr", vim.lsp.buf.remove_workspace_folder, "[W]orkspace [R]emove Folder")
    nmap("<leader>wl", function()
        P(vim.lsp.buf.list_workspace_folders())
    end, "[W]orkspace [L]ist Folders")

    -- Create a command `:Format` local to the LSP buffer
    vim.api.nvim_buf_create_user_command(bufnr, "Format", function(_)
        vim.lsp.buf.format()
    end, { desc = "Format current buffer with LSP" })
    nmap("<leader>cf", "<cmd>Format<cr>", "[C]ode [f]ormat")

    opts = opts or {}
    config = opts.config or {}
    filetypes = config.filetypes or {}
    for _k, lang in pairs(filetypes) do
        pcall(on_attach_overrides[lang], opts, bufnr)
    end
end

return {
    {
        "neovim/nvim-lspconfig",
        dependencies = {
            -- LSP / DAP / Formatter / Linter package manager
            { "williamboman/mason.nvim", config = true },
            { "williamboman/mason-lspconfig.nvim", dependencies = "williamboman/mason.nvim" },

            -- Useful status updates for LSP
            { "j-hui/fidget.nvim", config = true },

            -- Show signature help in the command line as you type
            { "ray-x/lsp_signature.nvim", event = "VeryLazy", config = true },
        },
        event = "VeryLazy",
    },
    {
        -- Autocompletion
        "hrsh7th/nvim-cmp",
        dependencies = {
            -- Snippet Engine & its associated nvim-cmp source
            "L3MON4D3/LuaSnip",
            "saadparwaiz1/cmp_luasnip",

            -- Adds LSP completion capabilities
            "hrsh7th/cmp-nvim-lsp",
            "hrsh7th/cmp-path",

            -- Adds a number of user-friendly snippets
            "rafamadriz/friendly-snippets",
        },
        event = "InsertEnter",
        config = function()
            local cmp = require("cmp")
            local luasnip = require("luasnip")
            require("luasnip.loaders.from_vscode").lazy_load()
            luasnip.config.setup({})

            cmp.setup({
                snippet = {
                    expand = function(args)
                        luasnip.lsp_expand(args.body)
                    end,
                },
                completion = {
                    completeopt = "menuone,noinsert",
                },
                mapping = cmp.mapping.preset.insert({
                    ["<C-n>"] = cmp.mapping.select_next_item(),
                    ["<C-p>"] = cmp.mapping.select_prev_item(),
                    ["<C-b>"] = cmp.mapping.scroll_docs(-4),
                    ["<C-f>"] = cmp.mapping.scroll_docs(4),
                    ["<C-Space>"] = cmp.mapping.complete({}),
                    ["<CR>"] = cmp.mapping.confirm({
                        behavior = cmp.ConfirmBehavior.Replace,
                        select = true,
                    }),
                    ["<Tab>"] = cmp.mapping(function(fallback)
                        if cmp.visible() then
                            cmp.select_next_item()
                        elseif luasnip.expand_or_locally_jumpable() then
                            luasnip.expand_or_jump()
                        else
                            fallback()
                        end
                    end, { "i", "s" }),
                    ["<S-Tab>"] = cmp.mapping(function(fallback)
                        if cmp.visible() then
                            cmp.select_prev_item()
                        elseif luasnip.locally_jumpable(-1) then
                            luasnip.jump(-1)
                        else
                            fallback()
                        end
                    end, { "i", "s" }),
                }),
                sources = {
                    { name = "nvim_lsp" },
                    { name = "luasnip" },
                    { name = "path" },
                    { name = "supermaven" },
                    {
                        name = "lazydev",
                        group_index = 0, -- set group index to 0 to skip loading LuaLS completions
                    },
                },
            })
        end,
    },
    {
        "williamboman/mason-lspconfig.nvim",
        config = function(_, opts)
            local servers = {
                -- clangd = {},
                gopls = {
                    gopls = {
                        completeUnimported = true,
                    },
                },
                -- pyright = {},
                -- rust_analyzer = {},
                -- tsserver = {},
                -- html = { filetypes = { "html", "twig", "hbs"} },

                lua_ls = {
                    Lua = {
                        workspace = { checkThirdParty = false },
                        telemetry = { enable = false },
                        -- NOTE: toggle below to ignore Lua_LS"s noisy `missing-fields` warnings
                        -- diagnostics = { disable = { "missing-fields" } },
                    },
                },
            }

            -- nvim-cmp supports additional completion capabilities, so broadcast that to servers
            local client_capabilities = vim.lsp.protocol.make_client_capabilities()
            local capabilities = require("cmp_nvim_lsp").default_capabilities(client_capabilities)

            local mason_lspconfig = require("mason-lspconfig")
            mason_lspconfig.setup()

            mason_lspconfig.setup_handlers({
                function(server_name)
                    local server_opts = servers[server_name] or {}
                    require("lspconfig")[server_name].setup({
                        capabilities = capabilities,
                        on_attach = on_attach,
                        settings = server_opts,
                        filetypes = server_opts.filetypes,
                    })
                end,
            })
        end,
        event = "VeryLazy",
    },
    -- The following are Lua dev plugins
    {
        "folke/lazydev.nvim",
        ft = "lua", -- only load on lua files
        opts = {
            library = {
                -- See the configuration section for more details
                -- Load luvit types when the `vim.uv` word is found
                { path = "luvit-meta/library", words = { "vim%.uv" } },
            },
        },
    },
    { "Bilal2453/luvit-meta", lazy = true }, -- optional `vim.uv` typings
    { "L3MON4D3/LuaSnip", lazy = true, build = "make install_jsregexp" }, -- optional snippets
}
