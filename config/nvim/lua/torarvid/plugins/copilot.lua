return {
    {
        "zbirenbaum/copilot.lua",
        cmd = "Copilot",
        event = "InsertEnter",
        enabled = false, -- Slow, or not working...
        config = function()
            require("copilot").setup({
                panel = {
                    enabled = true,
                    auto_refresh = false,
                    keymap = {
                        jump_prev = "[[",
                        jump_next = "]]",
                        accept = "<CR>",
                        refresh = "gr",
                        open = "<M-CR>"
                    },
                    layout = {
                        position = "bottom", -- | top | left | right
                        ratio = 0.4
                    },
                },
                suggestion = {
                    enabled = true,
                    auto_trigger = false,
                    debounce = 75,
                    keymap = {
                        accept = "<C-C>",
                        accept_word = false,
                        accept_line = false,
                        next = "<C-N>",
                        prev = "<C-P>",
                        dismiss = "<C-D>",
                    },
                },
                filetypes = {
                    yaml = false,
                    markdown = false,
                    help = false,
                    gitcommit = false,
                    gitrebase = false,
                    hgcommit = false,
                    svn = false,
                    cvs = false,
                    ["."] = false,
                },
                copilot_node_command = 'node', -- Node.js version must be > 18.x
                server_opts_overrides = {},
            })
        end,
    },
    {
        "github/copilot.vim",
        init = function()
            vim.g.copilot_no_tab_map = true
        end,
        config = function()
            vim.keymap.set('i', '<C-C>', 'copilot#Accept()', {
                expr = true,
                replace_keycodes = false
            })
            vim.keymap.set('i', '<C-L>', '<Plug>(copilot-accept-word)')
            vim.keymap.set('i', '<C-S-L>', '<Plug>(copilot-accept-line)')
            vim.keymap.set('i', '<C-D>', '<Plug>(copilot-dismiss)')
        end,
        cmd = "Copilot",
        event = "InsertEnter",
        enabled = false,
    }
}
