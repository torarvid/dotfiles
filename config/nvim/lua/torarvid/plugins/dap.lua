return {
    {
        "mfussenegger/nvim-dap",
        config = function(opts)
            local dap = require("dap")

            -- Debugger for LLDB (LLVM languages like C, C++, Rust, Swift, etc.)
            dap.adapters.codelldb = {
                type = "server",
                port = "${port}",
                executable = {
                    command = "codelldb",
                    args = { "--port", "${port}" },
                },
            }

            dap.configurations.rust = {
                {
                    name = "Debug",
                    type = "codelldb",
                    request = "launch",
                    program = "target/debug/${workspaceFolderBasename}",
                    cwd = "${workspaceFolder}",
                    args = {},
                    stopOnEntry = false,
                },
                {
                    name = "Debug with args",
                    type = "codelldb",
                    request = "launch",
                    program = "target/debug/${workspaceFolderBasename}",
                    cwd = "${workspaceFolder}",
                    args = function()
                        return { vim.fn.input("Arguments: ") }
                    end,
                    stopOnEntry = false,
                },
            }
        end,
        keys = {
            { "<Leader>db", "<Cmd>lua require('dap').toggle_breakpoint()<CR>", { desc = "DAP: Toggle breakpoint" } },
            { "<Leader>dc", "<Cmd>lua require('dap').continue()<CR>", { desc = "DAP: Continue" } },
            { "<Leader>dO", "<Cmd>lua require('dap').step_out()<CR>", { desc = "DAP: Step out" } },
            { "<Leader>di", "<Cmd>lua require('dap').step_into()<CR>", { desc = "DAP: Step into" } },
            { "<Leader>do", "<Cmd>lua require('dap').step_over()<CR>", { desc = "DAP: Step over" } },
            { "<Leader>dr", "<Cmd>lua require('dap').repl.toggle()<CR>", { desc = "DAP: Toggle REPL" } },
            { "<Leader>ds", "<Cmd>lua require('dap').stop()<CR>", { desc = "DAP: Stop" } },
        },
    },
    {
        "rcarriga/nvim-dap-ui",
        dependencies = { "mfussenegger/nvim-dap", "nvim-neotest/nvim-nio" },
        config = true,
        keys = {
            { "<Leader>du", "<Cmd>lua require('dapui').toggle()<CR>", { desc = "DAP: Toggle UI" } },
        },
    },
    {
        "leoluz/nvim-dap-go",
        dependencies = { "mfussenegger/nvim-dap" },
        config = function()
            local dap = require("dap")

            -- Debugger for Delve (Golang)
            dap.adapters.delve = {
                type = "server",
                port = "${port}",
                executable = {
                    command = "dlv",
                    args = { "dap", "-l", "127.0.0.1:${port}" },
                },
            }

            -- https://github.com/go-delve/delve/blob/master/Documentation/usage/dlv_dap.md
            dap.configurations.go = {
                {
                    type = "delve",
                    name = "Debug",
                    request = "launch",
                    program = "${file}",
                },
                {
                    type = "delve",
                    name = "Debug test", -- configuration for debugging test files
                    request = "launch",
                    mode = "test",
                    program = "${file}",
                },
                -- works with go.mod packages and sub packages
                {
                    type = "delve",
                    name = "Debug test (go.mod)",
                    request = "launch",
                    mode = "test",
                    program = "./${relativeFileDirname}",
                },
            }
        end,
    },
}
